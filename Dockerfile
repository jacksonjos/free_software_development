FROM continuumio/miniconda3:4.4.10

MAINTAINER Jackson Souza "jackson@ime.usp.br"

RUN echo "\ndeb-src http://archive.ubuntu.com/ubuntu/ xenial main" >> /etc/apt/sources.list

RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 40976EAF437D05B5
RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 3B4FE6ACC0B21F32

RUN apt-get update

# It's also needed to install python3.5 package, it's already installed at
# miniconda3 image
RUN apt-get install -y git-core vim

# Instala as dependências para compilar o Python para desenvolvimento
RUN apt-get build-dep -y python3.5

ARG yourname
ENV yourname=${yourname}

RUN mkdir -p /pandas-${yourname}
WORKDIR /pandas-${yourname}

CMD ["/bin/bash"]
