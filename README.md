# About

This repository contains presentations and a report as part of tasks and assignments of Free software (and open source) development.

# Presentations

*  'Virtualization and containers' is about an overview of virtualization technologies like Virtual Machines, e.g. Virtualbox, Containers, e.g. Docker, and so on.
* 'Free software in government' is about Brazilian government policy about free software adoption and its development in state organizations.
* 'Propriedade Intelectual e Lei de Software' (written in Portuguese) is about intellectual property like author rights, e.g. copyright and Creative Commons, and software rights, e.g. copyright and Free Software and Open Source licenses.
* 'FLOSS in Data Science research' is about research in Data Science including publications about Data Science tools and the creation and use of Data Science Free Software and Open Source.

Also, there is a report (in Portuguese) about my experience and contribution to Pandas, a Data Analysis Python module that will be available in June 17 2018.

All presentations are in its respective directories (folders) in the repository and as you can see they are licensed at Creative Commons. So, use it at your will.
