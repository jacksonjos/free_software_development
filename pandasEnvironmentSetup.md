# Instructions to contribute to pandas-dev Python module inside a Docker container

This tutorial is intended to Debian and Debian like distributions. If you are using another Unix distribution you may have to make changes in this tutorial, e.g. the Docker CE version that will be installed.  

To follow the instructions in this tutorial you need a Github account, a Dockerhub account and install Docker CE: https://docs.docker.com/install/linux/docker-ce/debian/#set-up-the-repository .  
Also, after install Docker CE you may need to add your user to the docker group typing: `sudo usermod -a -G docker $USER`.
If you don't want to use a Docker container to set up the environment for installing pandas-dev you also have to install git, anaconda and vim.

All commands are executed as root because the Docker image is not build with a common Linux user, but the only command that would need root permission is chmod over '/opt' directory because the root user is its owner. The other commands may be run without sudo if you have a standard Linux user.

# Fork pandas project on Github
    1. Login on github.com using your account

    2. Go to pandas project page: https://github.com/pandas-dev/pandas

    3. Click on Fork button in the upper right side of Pandas project page

# Create and build the Docker container for your pandas development repository
    1. Create directory to where the pandas repository will be cloned to in the host machine
    `$ mkdir pandas-jackson`

    2. Build Docker image
    `# docker build -t pandas_dev_i --build-arg yourname=jackson .`

    3. Create docker container with unamed docker volume and binds the pandas project host directory to pandas container directory/volume  
    `# docker run --name pandas_dev_c -v "$(pwd)/pandas-jackson":/pandas-jackson -i -t pandas_dev_i /bin/bash`

# Clone pandas development repository and configure your git inside the container
    1. Configure your e-mail and name to be identified in GitHub to be able to do commit changes and push them
    `$ git config --global user.mail "jacksonjsouza@gmail.com"`
    `$ git config --global user.name "Jackson Souza"`

    2. Generate a SSH key in the Docker container and place it on Github following the instructions in the following links:
    https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/
    https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/

    3. Fork the pandas-dev repository using your Github account  
    Go to https://github.com/pandas-dev/pandas and click the `Fork` button

    4. Clone pandas development repository
    `# git clone git@github.com:jacksonjos/pandas.git .`

    5. Connects your repository to the upstream (main project) pandas-dev repository.
    `# git remote add upstream https://github.com/pandas-dev/pandas.git`


# Configure anaconda to use a virtual environment and to install pandas
    1. To Make possible to use 'conda activate' command type the following
    <!-- # rm /etc/profile.d/conda.sh && ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh -->
    `# chmod +x /opt/conda/etc/profile.d/conda.sh`  
    `# echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc`
    <!-- `# echo "conda activate" >> ~/.bashrc` -->

    2. Install IPython interpretor and debugger to help debugging pandas-dev errors
    `# conda install ipython`

    3. Create and activate the build environment
    `# conda env create -f ci/environment-dev.yaml && source activate pandas-dev`

    4. Build and install pandas
    `# python setup.py build_ext --inplace -j 4 && python -m pip install -e .`

    5. Install the rest of the optional dependencies
    `# conda install -c defaults -c conda-forge --file=ci/requirements-optional-conda.txt`

# Instruction to enter and exit the virtual environment and stop, start and enter in the container
    1. To activate pandas-dev:
    `# conda activate pandas-dev`  

    2. To deactivate pandas-dev:
    `# conda deactivate`  

    3. To stop this container type:
    `# docker stop pandas_dev_c`

    4. To start this container type:
    `# docker start pandas_dev_c`

    5. To enter the container type:
    `# docker exec -ti pandas_dev_c /bin/bash`

    6. To exit the container type:
    `# exit`


# Update git repository to be up to date to the official/main Pandas repository
    1. Download changes made in the main/upstream repository:
    `# git fetch upstream`

    2. Checkout repository to local master branch
    `# git checkout master`

    3. Merge downloaded changes at the local master branch
    `# git merge upstream/master`


# Doing contributions using a branch
    1. Create a branch and checkout to it
    `git checkout -b shiny-new-feature`

    2. Commit the changes you have made in `shiny-new-feature`

    3. Rebase your commit using:
    `git rebase -i HEAD~#`
    where # is the number of commits you want to merge in only one. Be careful when rebasing to don't screw up your work

    4. Update the git repository as said in the topic above

    5. Checkout to the branch you are contributing to
    E.g: `git checkout shiny-new-feature`

    6. Rebase your branch to the master branch:
    `git rebase -i master`

    7. Push your changes
    `git push origin shiny-new-feature -f`


<!-- # Ver também lugar mais adequado para manter estas instruções de uso. -->


# References:
[1] https://help.github.com/articles/syncing-a-fork/  
[2] https://pandas.pydata.org/pandas-docs/stable/contributing.html  
[3] https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/
